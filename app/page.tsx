import { getFeedV1 } from './action';
import FeedList from './feed-list';

export default async function Home() {
  const feed = await getFeedV1({ userQuery: '飲料' });
  const feedCount =
    feed.status === 'success'
      ? parseInt(feed.data.subtitle.text.split(' ')[0])
      : 80;
  const allFeed = await getFeedV1({
    userQuery: '飲料',
    pageInfo: {
      offset: 1,
      pageSize: feedCount,
    },
  });
  return (
    <main className='h-full overflow-auto bg-slate-600 p-4 text-gray-300 md:p-20'>
      <FeedList feed={allFeed} />
    </main>
  );
}
