'use client';

import Image from 'next/image';
import { HTMLAttributes, useMemo, useState } from 'react';
import { Feed } from './action';

const TEA_SHOP_FILTER = [
  '貓頭鷹茶森林',
  '樂迷手作茶飲',
  'CoCo都可',
  '七盞茶',
  '沐荼寺',
  '山水茶堂',
  '鮮芋仙',
  '珍煮丹',
  '迷客夏 Milksha',
  '茶牧場',
  '得正 OOLONG TEA PROJECT',
  '功夫茶KUNGFUTEA',
  '瀚漟茶坊',
  '可不可熟成紅茶',
  '速貢',
  'COMEBUY',
  '甘蔗媽媽',
  '吳家紅茶冰',
  'CoCo都可',
  '新井茶',
  '大苑子',
];

interface FeedListProps extends HTMLAttributes<HTMLElement> {
  feed: Feed;
}
export default function FeedList({ className, children, feed }: FeedListProps) {
  const teaShopFeedData = useMemo(() => {
    if (feed?.status === 'success') {
      const filteredFeedItems = feed.data.feedItems.filter(item =>
        TEA_SHOP_FILTER.some(shop => item.store.title.text.includes(shop))
      );

      return { ...feed.data, feedItems: filteredFeedItems };
    }
    return null;
  }, [feed]);
  const [selectedShop, setSelectedShop] = useState('');
  const [isDrawing, setIsDrawing] = useState(false);

  const handleDraw = () => {
    if (isDrawing || !teaShopFeedData) return;

    setIsDrawing(true);
    let current = 0;
    const shops = teaShopFeedData.feedItems.map(item => item.store.title.text);
    const drawInterval = setInterval(
      () => {
        setSelectedShop(shops[current % shops.length]);
        current++;

        // 調整這裡來控制轉幾圈
        if (current > shops.length * 1) {
          clearInterval(drawInterval);
          setIsDrawing(false);
          // 隨機選擇最終店家
          setSelectedShop(shops[Math.floor(Math.random() * shops.length)]);
        }
      },
      // 隨著時間逐漸增加間隔時間
      100 + current * 10
    );
  };

  return (
    <div className='flex flex-col gap-4'>
      <button
        className='mb-2 me-2 rounded-lg bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-gradient-to-br focus:outline-none focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-800'
        onClick={handleDraw}
      >
        抽抽
      </button>
      <div className={'flex flex-wrap gap-4'}>
        {teaShopFeedData?.feedItems?.map(item => (
          <div
            key={item.uuid}
            className={`flex flex-col justify-between border ${
              item.store.title.text === selectedShop
                ? 'border-4 border-red-500'
                : 'bg-slate-800'
            }`}
            style={{ width: `${item.store.image.items[5]?.width ?? 300}px` }}
          >
            <p className='min-h-12 flex items-center justify-center'>
              {item.store.title.text}
            </p>
            <Image
              src={item.store.image.items[5]?.url ?? ''}
              width={item.store.image.items[5]?.width ?? 300}
              height={item.store.image.items[5]?.height ?? 300}
              alt={'store image'}
              priority
            />
          </div>
        ))}
      </div>
    </div>
  );
}
