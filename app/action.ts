'use server';

/** Uber Eat的API Response */
type FeedItem = {
  uuid: string;
  store: {
    image: {
      items: {
        url: string;
        width: number;
        height: number;
      }[];
    };
    title: {
      text: string;
    };
  };
};
type SuccessFeed = {
  status: 'success';
  data: { feedItems: FeedItem[]; subtitle: { text: string } };
};
type FailedFeed = { status: 'failed'; message: string };
export type Feed = SuccessFeed | FailedFeed;

const ADDRESS_COOKIE =
  'uev2.loc=%7B%22address%22%3A%7B%22address1%22%3A%22%E6%96%87%E5%8C%96%E4%BA%8C%E8%B7%AF%E4%B8%80%E6%AE%B5266%E8%99%9F%22%2C%22address2%22%3A%22%E6%96%B0%E5%8C%97%E5%B8%82%E6%9E%97%E5%8F%A3%E5%8D%80%22%2C%22aptOrSuite%22%3A%22%22%2C%22eaterFormattedAddress%22%3A%22244%E5%8F%B0%E7%81%A3%E6%96%B0%E5%8C%97%E5%B8%82%E6%9E%97%E5%8F%A3%E5%8D%80%E6%96%87%E5%8C%96%E4%BA%8C%E8%B7%AF%E4%B8%80%E6%AE%B5266%E8%99%9F%22%2C%22subtitle%22%3A%22%E6%96%B0%E5%8C%97%E5%B8%82%E6%9E%97%E5%8F%A3%E5%8D%80%22%2C%22title%22%3A%22%E6%96%87%E5%8C%96%E4%BA%8C%E8%B7%AF%E4%B8%80%E6%AE%B5266%E8%99%9F%22%2C%22uuid%22%3A%22%22%7D%2C%22latitude%22%3A25.0734155%2C%22longitude%22%3A121.3725397%2C%22reference%22%3A%22ChIJQ-DVKN-mQjQRaq8ykcpH1sc%22%2C%22referenceType%22%3A%22google_places%22%2C%22type%22%3A%22google_places%22%2C%22addressComponents%22%3A%7B%22city%22%3A%22%E4%BB%81%E6%84%9B%E9%87%8C%22%2C%22countryCode%22%3A%22TW%22%2C%22firstLevelSubdivisionCode%22%3A%22%E6%96%B0%E5%8C%97%E5%B8%82%22%2C%22postalCode%22%3A%22244%22%7D%2C%22categories%22%3A%5B%22street_address%22%5D%2C%22originType%22%3A%22user_autocomplete%22%2C%22source%22%3A%22manual_auto_complete%22%7D;';

type getFeedV1Props = {
  userQuery: string;
  pageInfo?: { offset?: number; pageSize?: number };
};
export async function getFeedV1({
  userQuery,
  pageInfo,
}: getFeedV1Props): Promise<Feed> {
  const data = {
    userQuery,
    searchSource: 'SEARCH_BAR',
    ...(pageInfo && { pageInfo }),
  };

  try {
    const response = await fetch(
      'https://www.ubereats.com/_p/api/getFeedV1?localeCode=tw',
      {
        method: 'POST',
        body: JSON.stringify(data),
        headers: new Headers({
          'Content-Type': 'application/json',
          'X-Csrf-Token': 'x',
          Cookie: ADDRESS_COOKIE,
        }),
      }
    );

    if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
    return await response.json();
  } catch (error) {
    console.error('Error fetching data: ', error);
    return { status: 'failed', message: (error as Error).message };
  }
}
